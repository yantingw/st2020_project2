# [Project 2 - GUI testing](https://hackmd.io/@xlYUTygoRkyuQQlwXuWDWQ/r1UU-rOL8)

Student ID: {**R08942155**}

<hr />

## Get Started

* `git clone https://gitlab.com/r07943154/st2020_project2.git`
* `cd st2020_project2`
* `yarn install`
* `yarn start`
    * visit your web application at http://localhost:3000
* Open another terminal, and run `yarn test`.
* Create a new public repository called `st2020_project2` and change remote.


## TODO 

Complete the 15 test cases in the `test/ui.test.js`, the screenshot should be  stored in the `test/screenshots/` folder.

1. [Content] Login page title should exist
2. [Content] Login page join button should exist
3. [Screenshot] Login page
5. [Screenshot] Welcome message
    * <img src="https://i.imgur.com/3X8uyYB.png" width="50%" />
7. [Screenshot] Error message when you login without user and room name
    * <img src="https://i.imgur.com/6RGIPoD.png" width="50%" />
9. [Content] Welcome message
10. [Behavior] Type user name
11. [Behavior] Type room name
12. [Behavior] Login
13. [Behavior] Login 2 users
14. [Content] The "Send" button should exist
15. [Behavior] Send a message
16. [Behavior] John says "Hi" and Mike says "Hello"
17. [Content] The "Send location" button should exist
19. [Behavior] Send a location message

