const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true;

// 1 - example
test('[Content] Login page title should exist', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser }); // show or not

    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();

})


// 2 
test('[Content] Login page join button should exist', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let button_name = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button_name).toBe('Join');
    await browser.close();

})

// 3 - example
test('[Screenshot] Login page', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({ path: 'test/screenshots/login.png' });
    await browser.close();

})

4
test('[Screenshot] Welcome message', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.waitFor(500); // 等待0.5秒
    page.type('body > div > form > div:nth-child(2) > input[type=text]', 'bbb');
    await page.waitFor(500); // 等待0.5秒
    page.type('body > div > form > div:nth-child(3) > input[type=text]', 'aaa');
    await page.waitFor(500); // 等待0.5秒
    page.click('body > div > form > div:nth-child(4) > button');
    await page.waitFor(700); // 等待0.5秒

    await page.screenshot({ path: 'test/screenshots/Welcome message.png' });
    await browser.close();

})

// 5
test('[Screenshot] Error message when you login without user and room name', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    page.click('body > div > form > div:nth-child(4) > button');
    await page.waitFor(500); // 等待一秒
    await page.screenshot({ path: 'test/screenshots/Error message when you login without user and room name.png' });
    await browser.close();

})


//6
test('[Content] Welcome message', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.waitFor(500); // 等待0.5秒
    page.type('body > div > form > div:nth-child(2) > input[type=text]', 'bbb');
    await page.waitFor(500); // 等待0.5秒
    page.type('body > div > form > div:nth-child(3) > input[type=text]', 'aaa');
    await page.waitFor(500); // 等待0.5秒
    page.click('body > div > form > div:nth-child(4) > button');
    await page.waitFor(700); // 等待0.5秒
    await page.waitForSelector('#messages > li > div:nth-child(2) > p');
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li > div:nth-child(2) > p').innerHTML;
    });
    expect(message).toBe('Hi bbb, Welcome to the chat app');
    await page.waitFor(500);
    await browser.close();


})

//7 - example
test('[Behavior] Type user name', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();

})

// 8
test('[Behavior] Type room name', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'ROOM', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('ROOM');

    await browser.close();

})

// // 9 - example
test('[Behavior] Login', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();

    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaa', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R9', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });

    await page.waitForSelector('#users > ol > li');
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('aaa');

    await browser.close();

})

// 10
test('[Behavior] Login 2 users', async() => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const page_1 = await browser1.newPage();
    await page_1.goto(url);
    await page_1.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaa', { delay: 100 });
    await page_1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', { delay: 100 });
    await page_1.keyboard.press('Enter', { delay: 100 });

    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page_2 = await browser2.newPage();
    await page_2.goto(url);
    await page_2.type('body > div > form > div:nth-child(2) > input[type=text]', 'bbb', { delay: 100 });
    await page_2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R10', { delay: 100 });
    await page_2.keyboard.press('Enter', { delay: 100 });

    await page_1.waitForSelector('#users > ol > li:nth-child(1)');
    let b1_m1 = await page_1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    await page_1.waitForSelector('#users > ol > li:nth-child(2)');
    let b1_m2 = await page_1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    await page_2.waitForSelector('#users > ol > li:nth-child(1)');
    let b2_m1 = await page_2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    await page_2.waitForSelector('#users > ol > li:nth-child(2)');
    let b2_m2 = await page_2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(b1_m1).toBe('aaa');
    expect(b1_m2).toBe('bbb');
    expect(b2_m1).toBe('aaa');
    expect(b2_m2).toBe('bbb');


})

// 11
test('[Content] The "Send" button should exist', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaa', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R11', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitForSelector('#users > ol > li');

    let send_button = await page.$eval('#message-form > button', (content) => content.innerHTML);
    expect(send_button).toBe('Send');
    await browser.close();

})

12
test('[Behavior] Send a message', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaa', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R12', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitForSelector('#users > ol > li');

    await page.type('#message-form > input[type="text"]', 'hellooo', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitForSelector('#messages > li:nth-child(2)');

    let new_message = await page.$eval('#messages > li:nth-child(2) > div.message__body > p', (content) => content.innerHTML);
    expect(new_message).toBe('hellooo');
    await browser.close();

})

13
test('[Behavior] John says "Hi" and Mike says "Hello"', async() => {
    //john login
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const page_1 = await browser1.newPage();
    await page_1.goto(url);
    await page_1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', { delay: 100 });
    await page_1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', { delay: 100 });
    await page_1.keyboard.press('Enter', { delay: 100 });
    // mike login
    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page_2 = await browser2.newPage();
    await page_2.goto(url);
    await page_2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', { delay: 100 });
    await page_2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R13', { delay: 100 });
    await page_2.keyboard.press('Enter', { delay: 100 });
    //john send hi
    await page_1.type('#message-form > input[type="text"]', 'Hi', { delay: 100 });
    await page_1.keyboard.press('Enter', { delay: 100 });

    //mike send hello
    await page_2.type('#message-form > input[type="text"]', 'Hello', { delay: 100 });
    await page_2.keyboard.press('Enter', { delay: 100 });


    //check page_1
    await page_1.waitForSelector('#messages > li:nth-child(4)');
    let first_name = await page_1.$eval('#messages > li:nth-child(3) > div.message__title > h4', (content) => content.innerHTML);
    expect(first_name).toBe('John');
    let first_message = await page_1.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    expect(first_message).toBe('Hi');
    let second_name = await page_1.$eval('#messages > li:nth-child(4) > div.message__title > h4', (content) => content.innerHTML);
    expect(second_name).toBe('Mike');
    let second_message = await page_1.$eval('#messages > li:nth-child(4) > div.message__body > p', (content) => content.innerHTML);
    expect(second_message).toBe('Hello');

    await page_2.waitForSelector('#messages > li:nth-child(3)');
    let first_name2 = await page_2.$eval('#messages > li:nth-child(2) > div.message__title > h4', (content) => content.innerHTML);
    expect(first_name2).toBe('John');
    let first_message2 = await page_2.$eval('#messages > li:nth-child(2) > div.message__body > p', (content) => content.innerHTML);
    expect(first_message2).toBe('Hi');
    let second_name2 = await page_2.$eval('#messages > li:nth-child(3) > div.message__title > h4', (content) => content.innerHTML);
    expect(second_name2).toBe('Mike');
    let second_message2 = await page_2.$eval('#messages > li:nth-child(3) > div.message__body > p', (content) => content.innerHTML);
    expect(second_message2).toBe('Hello');

    await browser1.close();
    await browser2.close();

})


14
test('[Content] The "Send location" button should exist', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaa', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R14', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitForSelector('#users > ol > li');

    let send_button = await page.$eval('#send-location', (content) => content.innerHTML);
    expect(send_button).toBe('Send location');
    await browser.close();


})

// 15
test('[Behavior] Send a location message', async() => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'aaa', { delay: 100 });
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R15', { delay: 100 });
    await page.keyboard.press('Enter', { delay: 100 });
    await page.waitForSelector('#users > ol > li');
    const client = await page.target().createCDPSession();
    await client.send('Emulation.setGeolocationOverride', {
        latitude: 25,
        longitude: 121.5,
        accuracy: 100,
    });
    //await page.click('button[id="send-location"]');
    await page.$eval('#send-location', (content) => content.click());
    //await page.click('button[id="send-location"]');
    await page.waitForSelector('#messages > li:nth-child(2) > div.message__body > div > iframe');
    await page.evaluate('window.confirm = () => true')
    let my_location = await page.$eval('#messages > li:nth-child(2) > div.message__body > div > iframe', (content) => content.href);
    expect(my_location).toBe('https:&#x2F;&#x2F;www.google.com&#x2F;maps?q&#x3D;25,121.5');


})